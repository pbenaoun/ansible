#https://hub.docker.com/_/centos
#docker build --rm -t my-alpine .
#docker run --rm -it my-alpine bash

FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive

ENV TZ=Europe/Paris

RUN DEBIAN_FRONTEND="noninteractive" apt-get update && apt install -y ssh ansible git vim nano python3.8 python3-pip python3-venv dos2unix zip python-is-python3 && rm -rf /var/lib/apt/lists/*
RUN pip3 install botocore boto3
RUN mkdir -p /root/.ssh && touch /root/.ssh/id_rsa && chmod 400 /root/.ssh/id_rsa

CMD tail -f /dev/null